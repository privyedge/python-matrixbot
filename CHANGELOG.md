# Changelog

## [Unreleased]

## [0.0.7] - 2019-07-12
### Added
- Unit tests!
### Changed
- Cache.query() returns value only, not the key as well
- Deprecate use of `MatrixClient.get_rooms()`

## [0.0.6] - 2019-07-10
### Added
- Add `CHANGELOG.md`
### Changed
- Raise NotImplementedError for `process_event()`

## [0.0.5] - 2019-07-01
### Added
- Add `start()` method as shortcut to begin listening and enter loop

## [0.0.4] - 2019-07-01
### Changed
- Set required python version to ~=3.6 due to use of f-strings
- Add package dependencies for `Markdown` and `matrix_client`

## [0.0.3] - 2019-07-01
### Fixed
- Disable sqlite3 `check_same_thread` flag to allow multiple threads to access a common in-memory database

## [0.0.2] - 2019-07-01
### Changed
- Refactor Gitlab CI pipeline to upload to PyPI only after a successful build

### Fixed
- Fix mis-named call to `process_event()`

## [0.0.1] - 2019-06-30
### Added
- First release on PyPI as `python-matrixbot`

[Unreleased]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.5...master
[0.0.1]: https://gitlab.com/gibberfish/python-matrixbot/-/tags/0.0.1
[0.0.2]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.1...0.0.2
[0.0.3]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.2...0.0.3
[0.0.4]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.3...0.0.4
[0.0.5]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.4...0.0.5
[0.0.6]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.5...0.0.6
[0.0.7]: https://gitlab.com/gibberfish/python-matrixbot/compare/0.0.6...0.0.7
